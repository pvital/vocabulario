# Wordlist - Termos mais comuns utilizados e suas traduções

## Equipe brasileira de tradução

### Apresentação

Esta é uma tabela colaborativa baseada na wordlist do sistema DDTP para tradução
de descrição de pacotes. O objetivo é diminuir as dúvidas que os tradutores tem
em relação a tradução de alguns termos e também manter um padrão de tradução.

### Modo de uso e recomendações

Esta é uma tabela colaborativa, todos que possuem login no sistema salsa podem
contribuir e recomendamos fortemente que todos os membros da equipe realizem o
cadastro para tal. Sugerimos que observem o padrão utilizado na construção da
tabela e sigam o mesmo. Estamos abertos a sugestões de mudança, basta enviar um
e-mail para a lista propondo e iremos debater. A tabela pode ser usada para
qualquer tipo de tradução, páginas da web, wiki, descrição de pacotes, pacotes
e etc.

Caso tenha dúvida em relação a algum termo e sua tradução, envie um e-mail para
a lista e peça a opinião dos demais membros da equipe para que possamos juntos
definir qual a melhor opção.

Original   | Tradução | Exemplo de uso, comentários
--------- | ------ | ------
ABI | Interface Binária de Aplicação (Application Binary Interface) | 
ack, acknowledgment | notificação, reconhecimento | 
advisory | aviso | lit: consultoria; observar o contexto
a.k.a. | também conhecido como | 
API | Interface de Programação de Aplicação (Application Programming Interface) | 
applet  | miniaplicativo | 
artwork | ilustração | 
backbone | backbone | lit: espinha dorsal
backend | mecanismo, estrutura | 
backport | backport | 
basename | nome do arquivo sem a extensão | 
big-endian | extremidade maior primeiro | ver: endianness
binding | vínculo | 
Birds of a Feather (BOF) | Birds of a Feather (BOF) | complemente com: reunião informal, confraternização, "desconferência"
blacklist | lista de bloqueio | cuidado com terminologia discriminatória
Blend | Blend | ver: Pure Blend
bookmark | ? | GNOME: marcador
boot | boot, inicialização | 
bootloader | bootloader | 
bootstrap | bootstrap | 
box | máquina | lit: caixa; referência às máquinas dos portes
Braille display | linha braille | 
browser | navegador | 
bug tracking system | sistema de acompanhamento de bugs | 
bug-tracking-system | sistema de acompanhamento de bugs | 
bugtrackingsystem | sistema de acompanhamento de bugs | 
build | construção (build) | 
built in | embutido | 
built-in | embutido | 
builtin | embutido | 
CD-ROM | CD-ROM | 
changelog | changelog, registro de mudanças | se for o nome do arquivo, não traduzir
character | caractere | 
character set | conjunto de caracteres | 
character-set | conjunto de caracteres | 
characterset  | conjunto de caracteres | 
charset | conjunto de caracteres | forma abreviada de character set
chat  | ? | Sugestão: bate-papo
claws mail mailer  | Claws Mail | 
claws-mail-mailer  | Claws Mail | 
clawsmailmailer | Claws Mail | 
code folding | dobrar blocos de código | 
code-folding | dobrar blocos de código | 
codefolding | dobrar blocos de código | 
community-driven distribution | distribuição orientada pela comunidade | dirigida, com decisões baseadas na comunidade
concatenative speech synthesis | síntese de fala por concatenação | 
consistent | coerente | 
copyright | copyright | possibilidade: direito de autor
cross platform | interplataforma | 
cross-platform | interplataforma | 
crossplatform | interplataforma | 
custom | personalizar | 
custom debian distribution | distribuição Debian personalizada | 
custom-debian-distribution | distribuição Debian personalizada | 
customdebiandistribution | distribuição Debian personalizada | 
daemon | daemon | 
dead | abandonado, inativo | lit.: morto
deadkey | tecla morta | 
Debian Free Software Guidelines (DFSG) | Definição Debian de Software Livre (DFSG) | 
debugging | depuração | 
decrypt | descriptografar | 
delete | excluir | 
deprecate | tornar obsoleto | 
deprecated | obsoleto | 
derivative | derivado | "...distribuição derivada do Debian."
desktop | área de trabalho | 
desktop environment | ambiente de área de trabalho | 
desktop-environment | ambiente de área de trabalho | 
desktopenvironment | ambiente de área de trabalho | 
diff | diff (diferença entre arquivos), o comando diff | "há um diff (diferença entre arquivos)..."
display | exibir | 
dock | dock, doca | 
download | baixar | 
drag'n'drop | arrastar e soltar | 
driver | driver | às vezes "controlador", "controladora"
dual boot | dual boot | 
dropdown | suspenso(a) | 
dropdown list | lista suspensa | 
dropdown-list | lista suspensa | 
dropdownlist | lista suspensa | 
dummy package | pacote fictício (dummy) | 
dummy-package | pacote fictício (dummy) | 
dummypackage | pacote fictício (dummy) | 
e.g. | por exemplo, p. ex. | 
encrypt | criptografar | 
endianness | extremidade | ver: big-endian, little-endian
experimental | experimental | "a versão experimental..."
exploit | vulnerabilidade | 
eyetracker | sensor ocular | 
feature | recurso | 
feed | feed | 
fetch | busca e recuperação | de informações, pacotes, descrições...
find | localizar | 
flag | marcador (flag), sinal (flag) | lit: bandeira, sinal
for claws mail | para o Claws Mail | 
for-claws-mail | para o Claws Mail | 
forclawsmail | para o Claws Mail | 
framework | infraestrutura | 
free software | software livre | 
front end | interface | 
front-end | interface | 
frontend | interface | 
gathering | coleta | 
graphical user interface | interface gráfica do(a) usuário(a) | 
hack | hackear, alteração | às vezes: gambiarra
handbook | manual, guia, guia de referência | 
hardcode | inserir no próprio código | codificar o programa original em questão
harden | fortalecer | 
hardware | hardware | 
homepage | página web | 
host | servidor, hospedeiro | 
HOWTO | HOWTO | o documento HOWTO...
incoming | repositório de entrada (incoming) | 
infrastructure | infraestrutura | 
inline tag | tag de linha única, tag embutida | 
IRC (Internet Relay Chat) | IRC (Bate-papo de Retransmissão da Internet) | usualmente só consta a sigla
kernel | kernel, kernel (núcleo) | 
kernel panic | pânico de kernel | 
little-endian | extremidade menor primeiro | ver: endianness
live image | imagem de instalação "live", imagem "live" | 
logo | logotipo, logomarca | 
loopback | loopback | 
mailing list | lista de discussão, lista de e-mail | 
mailserver | servidor de e-mail | 
mainframe | mainframe | 
manpage | páginas man, manpage | 
master | primário, principal, iniciador, requisitante | cuidado com terminologia discriminatória
merchandise | materiais publicitários, de propaganda | também: brindes
merchandising | merchandising | indica o processo, não os produtos
mirroring | espelhamento | 
more information | mais informações | 
more-information | mais informações | 
moreinformation | mais informações | 
multi platform | multiplataforma | 
multi section | multisessão | 
multi-platform | multiplataforma | 
multi-section | multissessão | 
multiplatform | multiplataforma | 
multisection | multissessão | 
multithread | multithread | 
newline | quebra de linha | pode ser a ação ou o código que faz a quebra
oldstable | ? | antigaestável?
oldoldstable | ? | antigaantigaestável?
on the fly | ? | Sugestão: sob demanda
on-the-fly | ? | Sugestão: sob demanda
online | on-line | 
onthefly | ? | Sugestão: sob demanda
open source | código aberto | 
parse | analisar | 
parser | analisador | 
patch | correção (patch), modificação (patch) | 
performance | desempenho | 
pin | APT pinning: alfinetar | 
pipe | pipe, direcionamento | 
plug in | plug-in, extensão, complemento | 
plug-in | plug-in, extensão, complemento | 
plugin | plug-in, extensão, complemento | 
point release | versão pontual | "Debian 10.1, Debian 10.2..."
pool | depósito, reservatório | "The Debian package pool..."
pop up | janela instantânea | 
pop-up | janela instantânea | 
popcon | popcon | ver: Popularity Contest
Popularity Contest | Popularity Contest (Concurso de Popularidade) | 
popup | janela instantânea | 
port | porte | 
pre seed | pré-configuração | 
pre-seed | pré-configuração | 
preseed | pré-configuração | 
prompt | ? | manter como em inglês?
pseudopackage | pseudopacote | 
Pure Blend | Pure Blend | não traduzir
purge | expurgar | 
raw | não processado | 
refresh rate | taxa de atualização | 
relay | relay, retransmissão | SMTP relay, relay de e-mail
release | versão, lançamento | 
rendering | renderização, processamento | 
resize | redimensionar | 
run | executar | 
run time | execução | 
run-time | execução | 
runtime | execução | 
search | pesquisar | 
security issues | problemas de segurança | 
servlet | servlet | 
shell | shell, camada (biologia/química) | 
show | mostrar | 
site | site | 
slave | secundário, subordinado, alvo, requisitado, respondente | cuidado com terminologia discriminatória 
smart host | smarthost |
smarthost | smarthost |
snapshot | snapshot | imagem/cópia criada em determinado momento; pode ser o repositório do Debian com essas imagens
software | software | 
soname | soname | 
spider | robô da lista de discussão | importante verificar o contexto
spin box | seletor numérico | 
spin-box | seletor numérico | 
spinbox | seletor numérico | 
stable | estável (stable); a versão estável (stable) | 
stand alone | autônomo | 
stand-alone | autônomo | 
standalone | autônomo | 
string | texto, frase, sentença, sequência de caracteres |
swirl (Debian swirl) | espiral do Debian | 
switcher | ? | Sugestão: alternador
syntax highlight | realce de sintaxe | 
syntax highlighting | realce de sintaxe | 
syntax-highlight | realce de sintaxe | 
syntax-highlighting | realce de sintaxe | 
syntaxhighlight | realce de sintaxe | 
syntaxhighlighting | realce de sintaxe | 
system tray | bandeja de sistema | 
system-tray | bandeja de sistema | 
systemtray | bandeja de sistema | 
systray | bandeja de sistema | 
tarballs | arquivos tarball | 
team | time, equipe | 
template | modelo | documento de formato padronizado
testing | teste (testing); a versão teste (testing) | 
text folding | dobrar blocos de texto | 
text-folding | dobrar blocos de texto | 
textfolding | dobrar blocos de texto | 
text-to-speech (TTS) | texto-para-voz (TTS em inglês), texto-para-fala (TTS em inglês) | conversão texto-fala, texto-voz; pode aparecer sem hífens
thread | discussão | e-mails em sequência numa lista de discussão; também ver: multithread
timestamp | carimbo de tempo, marca de dia e hora | 
TODO | lista de tarefas, lista TODO | também: a fazer, a ser feito
toolchain | ? | Sugestão: cadeia base de ferramentas ("toolchain")
toolkit | kit de ferramentas | 
tooltip | dica de contexto, janela pop-up com dica | 
top-level directory | diretório mais alto na hierarquia | 
touchpad | touchpad | 
trackball | trackball | 
tracker | rastreador | 
tweak | ? | Sugestão: ajustar, refinar
tweaker | ? | Sugestão: refinador
typo | erro de digitação | 
unstable | instável (unstable); a versão instável (unstable) | 
update | atualização (update) | 
upgrade | atualização (upgrade) | 
upstream | ? | Sugestão: autor(a) original
uptime | uptime | tempo de operação com a máquina ligada
userland | espaço de usuário(a) |
whitelist | lista de permissão  | cuidado com terminologia discriminatória
visually impaired user | usuário(a) com deficiência visual | 
webboot | webboot | 
web page | página web | 
web site | site web | 
web-page | página web | 
web-site | site web | 
webpage | página web | 
website | site web | 
wiki | wiki, wiki do Debian | 
World Wide Web | World Wide Web | pode variar WWW ou www
x window system | x window system | 
x-window-system | X Window System | 
xwindowsystem | X Window System | 
